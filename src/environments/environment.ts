// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyB2__54qnEWs6UDEbFLld6x7xP9hSh5zOY',
    authDomain: 'projet-angular-ingesup.firebaseapp.com',
    databaseURL: 'https://projet-angular-ingesup.firebaseio.com',
    projectId: 'projet-angular-ingesup',
    storageBucket: 'projet-angular-ingesup.appspot.com',
    messagingSenderId: '333488750880'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
