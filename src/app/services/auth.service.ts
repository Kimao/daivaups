import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // Notre user
  user = {
    name: '',
    password: '',
    logged: false
  };

  check = {
    name: 'roger',
    password: '1234',
    firstname: 'roger',
    lastname: 'Jean-michel',
    email: 'jeanmichel@angular.com',
    phone: '06.06.06.06.06',
    description: 'je m\'appelle Roger j\'aime le pâté'
  };

  constructor(private http: HttpClient) { }

  checkUser(user: {name: string, password: string, logged: boolean}) {


    // Comparaison du form avec la "BDD"
    if (this.check.name === user.name && this.check.password === user.password) {

      // Succès
      this.user.name = user.name;
      this.user.password = user.password;
      this.user.logged = true;

      return true;
    }
  }

  isAuth() {
    return this.user.logged;
  }

  disconnect() {
    this.user = {
      name: '',
      password: '',
      logged: false
    };
  }
}
