import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  connect = false;

  // notre user local
  user = {
    name: '',
    password: '',
    logged: false
  };

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() { }

  onSubmit() {
    if (this.authService.checkUser(this.user)) {
      this.user.logged = true;
    }
  }

  displayLogin() {
    this.connect = true;
  }


  disconnect() {
    this.user.logged = false;
    this.authService.disconnect();
    this.router.navigate(['']);
    this.connect = false;
  }
}
